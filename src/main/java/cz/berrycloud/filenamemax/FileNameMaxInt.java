package cz.berrycloud.filenamemax;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import lombok.extern.java.Log;

@Log
public class FileNameMaxInt {

  public static int getNextNumber(final Path directory) {
    try (Stream<Path> stream = Files.walk(directory)) {
      return stream.peek(System.out::println).filter(p -> p.getFileName().toString().matches("^\\d+\\.md$"))
          .mapToInt(p -> Integer.valueOf(p.getFileName().toString().replaceFirst("\\.md$", ""))).max().orElse(0);
    } catch (final IOException exception) {
      log.severe(exception.getMessage());
    }
    return -1;
  }

}
