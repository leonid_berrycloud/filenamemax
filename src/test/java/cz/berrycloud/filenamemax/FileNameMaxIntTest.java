package cz.berrycloud.filenamemax;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

public class FileNameMaxIntTest {

	@Test
	public void test() throws Exception {

		final Path path = Paths.get(getClass().getClassLoader()
				.getResource("static/issues/").toURI());

		final int maxInt = FileNameMaxInt.getNextNumber(path);

		assertThat(maxInt, is(12));
	}

}
